import React from 'react';
import './App.css';
import ListCards from "./List/ListCards";

export default class App extends React.Component {
    render() {
        return (
            <ListCards />
        );
    }
}