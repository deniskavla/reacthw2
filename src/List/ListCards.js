// import React from 'react';
//
// class ListCards extends React.Component {
//     constructor(props) {
//         super(props);
//         this.clickurl = this.clickurl.bind(this);
//         this.result = undefined;
//     }
//
//     render() {
//         const list = this.result && this.result;
//
//         return (
//             <div>
//                 <button onClick={this.clickurl}>clickurl</button>
//                 {list}
//             </div>
//         );
//     }
//
//     clickurl(props) {
//
//         fetch("product.json")
//             .then(response => response.json())
//             .then(result => this.result = result.map(item => <li>{item.id}</li>))
//             .catch(e => console.log(e));
//
//
//     }
// }
//
// export default ListCards;
import React from "react";
import ProductCard from "../Product/ProductCard";
import s from "./StyleCardsList.module.css"

class ListCards extends React.Component {
    constructor(props) {
        super(props);
        this.state = {items: []}
    }

    componentDidMount() {
        fetch("/product.json")
            .then(response => response.json())
            .then(result => this.setState({items: result}))
    }

    render() {
        const {items} = this.state;
        return (
            <div>
                <h1>Оружейный магазин</h1>
                <i className={`fas fa-shopping-basket ${s.shoppingBasket}`}></i>
                <main className={s.styleCardsList}>

                    {items.length ?
                        items.map((product, id) =>
                            <ProductCard key={product.id}
                                id={product.id}
                                url={product.url}
                                nameProduct={product.nameProduct}
                                color={product.color}
                                priceProduct={product.priceProduct}
                                vendorCode={product.vendorCode}/>) : null
                    }
                </main>
            </div>
        );
    }
}

export default ListCards;