import React, {Fragment} from 'react';
import style from './ModalAddToCards.module.css'
import PropTypes from "prop-types";
import Button from "../Button/Button";

class ModalAddToCard extends React.Component {
    constructor(props) {
        super(props);
        console.log(this.props);
    }

    render() {
        console.log(this.props.actions);
        return (
            <div className={`${style.modalOverlay} `} onClick={this.props.bgrndClick}>
                <div className={style.modalWindow}>
                    <div className={style.modalHeader}>
                        <div className={style.modalHeaderTitle}>
                            {this.props.header}
                            <div className={style.closed} onClick={this.props.bgrndClick}>+</div>
                        </div>
                    </div>
                    <div className={style.modalFooter}>
                        {this.props.actions}
                    </div>
                </div>
            </div>
        )
    }
}

export default ModalAddToCard;
Button.propTypes = {
    bgrndClick: PropTypes.func,
    open: PropTypes.bool
};