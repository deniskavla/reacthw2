import React from 'react';
import s from './ProductCard.module.css'
import Button from "../Button/Button";
import ModalAddToCard from "../ConfirmModal/ModalAddToCard";

const getFaforitsByLocalStorage = () => JSON.parse(localStorage.getItem('Favorit'));
const getBasket = () => JSON.parse(localStorage.getItem('Basket'));

class ProductCard extends React.Component {
    state = {
        modalAddToCard: false,
        isFavorit:
            Array.isArray(getFaforitsByLocalStorage())
                ? JSON.parse(localStorage.getItem('Favorit'))
                    .find(item => item === this.props.id)
                : false,
        isBasket:
            Array.isArray(getBasket())
                ? JSON.parse(localStorage.getItem('Basket'))
                    .find(item => item === this.props.id)
                : false,
    };
    //для Закрытие модалки
    closeModal = (e) => {

        if (e.target !== e.currentTarget) {
            return;
        }
        this.setState({
            modalAddToCard: false,
        });
    };

    //// добавление в localStorage
    addLocalStorege = () => {
        let card = this.props.id;
        this.setState({
                isFavorit: !this.state.isFavorit
            }
        );
        if (Array.isArray(getFaforitsByLocalStorage()) && getFaforitsByLocalStorage().find(item => card === item)) {
            localStorage.setItem('Favorit', JSON.stringify(getFaforitsByLocalStorage().filter(item => item !== card)));
        } else {
            if (Array.isArray(getFaforitsByLocalStorage())) {
                localStorage.setItem('Favorit', JSON.stringify([...getFaforitsByLocalStorage(), card]));
            } else {
                localStorage.setItem('Favorit', JSON.stringify([card]))
            }
        }
    };

    addBasket = () => {
        let card = this.props.id;
        this.setState({
                isFavorit: !this.state.isFavorit
            }
        );
        console.log(getBasket());
        if (Array.isArray(getBasket()) && getBasket().find((item) => card === item)) {
            localStorage.setItem('Basket', JSON.stringify(getBasket().filter((item) => item != card)));
        } else {
            if (Array.isArray(getBasket())) {
                localStorage.setItem('Basket', JSON.stringify([...getBasket(), card]));
            } else {
                localStorage.setItem('Basket', JSON.stringify([card]))
            }
        }
    };

    addToCard = () => {
        this.setState({
            modalAddToCard: true,
        });
    };

    render() {
        const buttonProperty = [
            {id: 1, text: 'Add to cart', backgroundColor: '#1E1E20'},
        ];
        const {url, nameProduct, color, priceProduct, vendorCode} = this.props;
        console.log(this.state.modalAddToCard);
        const modal = this.state.modalAddToCard &&
            <ModalAddToCard
                bgrndClick={this.closeModal}
                header={'Вы хотите добавить данный товар в корзину?'}
                actions={[
                    <Button
                        style={{
                            position: 'static',
                            color: 'black',
                            padding: '19px',
                            margin: '10px'
                        }}
                        text='Да' key='1' onClick={(e) => {
                        this.addBasket();
                        this.closeModal(e);
                    }}/>,
                    <Button
                        style={{
                            position: 'static',
                            color: 'black',
                            padding: '15px',
                            margin: '10px'
                        }}
                        text='Нет' key='2' onClick={this.closeModal}/>
                ]}
            />;
        return (
            <span>
            <div className={s.productCard}>
                <Button
                    text={buttonProperty[0].text}
                    buttoncolor={buttonProperty[0].backgroundColor}
                    onClick={() => this.addToCard()}
                />
                <span className={s.productArtikul}>Артикул {vendorCode}</span>
                <img className='img' src={url} alt=""/>
                <i onClick={this.addLocalStorege}
                        className={`far fa-heart ${this.state.isFavorit ? s.active : ""}`}></i>
                <p className={s.productPrice}>Цена: <strong> {priceProduct}</strong></p>
                <h5>{nameProduct}</h5>
                <p>Цвет:{color}</p>

            </div>
                {modal}
        </span>
        )
    }
}

export default ProductCard;
