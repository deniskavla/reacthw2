import React from "react";
import PropTypes from 'prop-types';

class Button extends React.Component {
    render() {
        return (
            <button type='button'

                style={{
                    backgroundColor: this.props.buttoncolor,
                    position: 'absolute',
                    top: '10px',
                    right: '10px',
                    color: 'white',
                    ...this.props.style
                }}

                onClick={this.props.onClick}
            >{this.props.text}
            </button>
        )
    }
}

export default Button;
Button.propTypes = {
    buttoncolor: PropTypes.string,
    text: PropTypes.string,
    onClick:PropTypes.func
};